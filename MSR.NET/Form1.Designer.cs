﻿namespace MSR.NET
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportByProjIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reconfigDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configM2MToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showM2MConnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configPackingSolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showPackingSolutionConnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.lblMold = new System.Windows.Forms.Label();
            this.lblColor = new System.Windows.Forms.Label();
            this.lblFeature = new System.Windows.Forms.Label();
            this.lblGrpCode = new System.Windows.Forms.Label();
            this.lblFacility = new System.Windows.Forms.Label();
            this.chkbxSendEmail = new System.Windows.Forms.CheckBox();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDepth = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSeq = new System.Windows.Forms.TextBox();
            this.cbxColor = new System.Windows.Forms.ComboBox();
            this.cbxFeature = new System.Windows.Forms.ComboBox();
            this.cbxProfile = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbWetCast = new System.Windows.Forms.RadioButton();
            this.rbDryCast = new System.Windows.Forms.RadioButton();
            this.txtProjId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblUserPre = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dgvColor = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColor)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.menuStrip1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 25;
            this.splitContainer1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.adminToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportByProjIDToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // reportByProjIDToolStripMenuItem
            // 
            this.reportByProjIDToolStripMenuItem.Name = "reportByProjIDToolStripMenuItem";
            this.reportByProjIDToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.reportByProjIDToolStripMenuItem.Text = "Report by Proj ID";
            this.reportByProjIDToolStripMenuItem.Click += new System.EventHandler(this.reportByProjIDToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // adminToolStripMenuItem
            // 
            this.adminToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reconfigDBToolStripMenuItem});
            this.adminToolStripMenuItem.Name = "adminToolStripMenuItem";
            this.adminToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.adminToolStripMenuItem.Text = "Admin";
            // 
            // reconfigDBToolStripMenuItem
            // 
            this.reconfigDBToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configM2MToolStripMenuItem,
            this.configPackingSolutionToolStripMenuItem});
            this.reconfigDBToolStripMenuItem.Enabled = false;
            this.reconfigDBToolStripMenuItem.Name = "reconfigDBToolStripMenuItem";
            this.reconfigDBToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.reconfigDBToolStripMenuItem.Text = "Reconfig DB";
            // 
            // configM2MToolStripMenuItem
            // 
            this.configM2MToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showM2MConnToolStripMenuItem});
            this.configM2MToolStripMenuItem.Name = "configM2MToolStripMenuItem";
            this.configM2MToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.configM2MToolStripMenuItem.Text = "Config M2M";
            // 
            // showM2MConnToolStripMenuItem
            // 
            this.showM2MConnToolStripMenuItem.Name = "showM2MConnToolStripMenuItem";
            this.showM2MConnToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.showM2MConnToolStripMenuItem.Text = "Show M2M Conn";
            // 
            // configPackingSolutionToolStripMenuItem
            // 
            this.configPackingSolutionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showPackingSolutionConnToolStripMenuItem});
            this.configPackingSolutionToolStripMenuItem.Name = "configPackingSolutionToolStripMenuItem";
            this.configPackingSolutionToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.configPackingSolutionToolStripMenuItem.Text = "Config Packing Solution";
            // 
            // showPackingSolutionConnToolStripMenuItem
            // 
            this.showPackingSolutionConnToolStripMenuItem.Name = "showPackingSolutionConnToolStripMenuItem";
            this.showPackingSolutionConnToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.showPackingSolutionConnToolStripMenuItem.Text = "Show Packing Solution Conn";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.lblMold);
            this.splitContainer2.Panel1.Controls.Add(this.lblColor);
            this.splitContainer2.Panel1.Controls.Add(this.lblFeature);
            this.splitContainer2.Panel1.Controls.Add(this.lblGrpCode);
            this.splitContainer2.Panel1.Controls.Add(this.lblFacility);
            this.splitContainer2.Panel1.Controls.Add(this.chkbxSendEmail);
            this.splitContainer2.Panel1.Controls.Add(this.txtHeight);
            this.splitContainer2.Panel1.Controls.Add(this.label7);
            this.splitContainer2.Panel1.Controls.Add(this.txtDepth);
            this.splitContainer2.Panel1.Controls.Add(this.label6);
            this.splitContainer2.Panel1.Controls.Add(this.txtSeq);
            this.splitContainer2.Panel1.Controls.Add(this.cbxColor);
            this.splitContainer2.Panel1.Controls.Add(this.cbxFeature);
            this.splitContainer2.Panel1.Controls.Add(this.cbxProfile);
            this.splitContainer2.Panel1.Controls.Add(this.label5);
            this.splitContainer2.Panel1.Controls.Add(this.label4);
            this.splitContainer2.Panel1.Controls.Add(this.label3);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.rbWetCast);
            this.splitContainer2.Panel1.Controls.Add(this.rbDryCast);
            this.splitContainer2.Panel1.Controls.Add(this.txtProjId);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.lblUserName);
            this.splitContainer2.Panel1.Controls.Add(this.lblUserPre);
            this.splitContainer2.Panel1.Controls.Add(this.btnUpdate);
            this.splitContainer2.Panel1.Controls.Add(this.btnSend);
            this.splitContainer2.Panel1.Controls.Add(this.btnAdd);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(800, 421);
            this.splitContainer2.SplitterDistance = 115;
            this.splitContainer2.TabIndex = 0;
            // 
            // lblMold
            // 
            this.lblMold.AutoSize = true;
            this.lblMold.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMold.Location = new System.Drawing.Point(378, 5);
            this.lblMold.Name = "lblMold";
            this.lblMold.Size = new System.Drawing.Size(62, 22);
            this.lblMold.TabIndex = 26;
            this.lblMold.Text = "XXXX";
            // 
            // lblColor
            // 
            this.lblColor.AutoSize = true;
            this.lblColor.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColor.Location = new System.Drawing.Point(336, 5);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(36, 22);
            this.lblColor.TabIndex = 25;
            this.lblColor.Text = "XX";
            // 
            // lblFeature
            // 
            this.lblFeature.AutoSize = true;
            this.lblFeature.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeature.Location = new System.Drawing.Point(307, 5);
            this.lblFeature.Name = "lblFeature";
            this.lblFeature.Size = new System.Drawing.Size(23, 22);
            this.lblFeature.TabIndex = 24;
            this.lblFeature.Text = "X";
            // 
            // lblGrpCode
            // 
            this.lblGrpCode.AutoSize = true;
            this.lblGrpCode.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrpCode.Location = new System.Drawing.Point(265, 5);
            this.lblGrpCode.Name = "lblGrpCode";
            this.lblGrpCode.Size = new System.Drawing.Size(36, 22);
            this.lblGrpCode.TabIndex = 23;
            this.lblGrpCode.Text = "XX";
            // 
            // lblFacility
            // 
            this.lblFacility.AutoSize = true;
            this.lblFacility.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFacility.Location = new System.Drawing.Point(233, 5);
            this.lblFacility.Name = "lblFacility";
            this.lblFacility.Size = new System.Drawing.Size(21, 22);
            this.lblFacility.TabIndex = 22;
            this.lblFacility.Text = "1";
            // 
            // chkbxSendEmail
            // 
            this.chkbxSendEmail.AutoSize = true;
            this.chkbxSendEmail.Location = new System.Drawing.Point(567, 93);
            this.chkbxSendEmail.Name = "chkbxSendEmail";
            this.chkbxSendEmail.Size = new System.Drawing.Size(123, 17);
            this.chkbxSendEmail.TabIndex = 21;
            this.chkbxSendEmail.Text = "Send Email on Close";
            this.chkbxSendEmail.UseVisualStyleBackColor = true;
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(182, 63);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(62, 20);
            this.txtHeight.TabIndex = 20;
            this.txtHeight.TextChanged += new System.EventHandler(this.txtHeight_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(140, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "height";
            // 
            // txtDepth
            // 
            this.txtDepth.Location = new System.Drawing.Point(52, 63);
            this.txtDepth.Name = "txtDepth";
            this.txtDepth.Size = new System.Drawing.Size(62, 20);
            this.txtDepth.TabIndex = 18;
            this.txtDepth.TextChanged += new System.EventHandler(this.txtDepth_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "depth";
            // 
            // txtSeq
            // 
            this.txtSeq.Location = new System.Drawing.Point(330, 63);
            this.txtSeq.Name = "txtSeq";
            this.txtSeq.Size = new System.Drawing.Size(69, 20);
            this.txtSeq.TabIndex = 16;
            this.txtSeq.TextChanged += new System.EventHandler(this.txtSeq_TextChanged);
            // 
            // cbxColor
            // 
            this.cbxColor.FormattingEnabled = true;
            this.cbxColor.Location = new System.Drawing.Point(528, 35);
            this.cbxColor.Name = "cbxColor";
            this.cbxColor.Size = new System.Drawing.Size(162, 21);
            this.cbxColor.TabIndex = 15;
            this.cbxColor.SelectionChangeCommitted += new System.EventHandler(this.cbxColor_SelectionChangeCommitted);
            // 
            // cbxFeature
            // 
            this.cbxFeature.FormattingEnabled = true;
            this.cbxFeature.Location = new System.Drawing.Point(349, 35);
            this.cbxFeature.Name = "cbxFeature";
            this.cbxFeature.Size = new System.Drawing.Size(109, 21);
            this.cbxFeature.TabIndex = 14;
            this.cbxFeature.SelectionChangeCommitted += new System.EventHandler(this.cbxFeature_SelectionChangeCommitted);
            // 
            // cbxProfile
            // 
            this.cbxProfile.FormattingEnabled = true;
            this.cbxProfile.Location = new System.Drawing.Point(215, 35);
            this.cbxProfile.Name = "cbxProfile";
            this.cbxProfile.Size = new System.Drawing.Size(44, 21);
            this.cbxProfile.TabIndex = 13;
            this.cbxProfile.SelectionChangeCommitted += new System.EventHandler(this.cbxProfile_SelectionChangeCommitted);
            this.cbxProfile.Leave += new System.EventHandler(this.cbxProfile_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(274, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "next seq:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(489, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "color:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(274, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "feature desc:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(172, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "profile:";
            // 
            // rbWetCast
            // 
            this.rbWetCast.AutoSize = true;
            this.rbWetCast.Location = new System.Drawing.Point(77, 36);
            this.rbWetCast.Name = "rbWetCast";
            this.rbWetCast.Size = new System.Drawing.Size(69, 17);
            this.rbWetCast.TabIndex = 8;
            this.rbWetCast.Text = "Wet Cast";
            this.rbWetCast.UseVisualStyleBackColor = true;
            // 
            // rbDryCast
            // 
            this.rbDryCast.AutoSize = true;
            this.rbDryCast.Checked = true;
            this.rbDryCast.Location = new System.Drawing.Point(6, 36);
            this.rbDryCast.Name = "rbDryCast";
            this.rbDryCast.Size = new System.Drawing.Size(65, 17);
            this.rbDryCast.TabIndex = 7;
            this.rbDryCast.TabStop = true;
            this.rbDryCast.Text = "Dry Cast";
            this.rbDryCast.UseVisualStyleBackColor = true;
            this.rbDryCast.CheckedChanged += new System.EventHandler(this.rbDryCast_CheckedChanged);
            // 
            // txtProjId
            // 
            this.txtProjId.Location = new System.Drawing.Point(54, 6);
            this.txtProjId.Name = "txtProjId";
            this.txtProjId.Size = new System.Drawing.Size(100, 20);
            this.txtProjId.TabIndex = 6;
            this.txtProjId.TextChanged += new System.EventHandler(this.txtProjId_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Proj ID:";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(50, 94);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(67, 13);
            this.lblUserName.TabIndex = 4;
            this.lblUserName.Text = "lblUesrName";
            // 
            // lblUserPre
            // 
            this.lblUserPre.AutoSize = true;
            this.lblUserPre.Location = new System.Drawing.Point(12, 94);
            this.lblUserPre.Name = "lblUserPre";
            this.lblUserPre.Size = new System.Drawing.Size(32, 13);
            this.lblUserPre.TabIndex = 3;
            this.lblUserPre.Text = "User:";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(713, 66);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Visible = false;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(713, 37);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 1;
            this.btnSend.Text = "&Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(713, 8);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.dgvColor);
            this.splitContainer3.Size = new System.Drawing.Size(800, 302);
            this.splitContainer3.SplitterDistance = 564;
            this.splitContainer3.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(564, 302);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // dgvColor
            // 
            this.dgvColor.AllowUserToAddRows = false;
            this.dgvColor.AllowUserToDeleteRows = false;
            this.dgvColor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvColor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvColor.Location = new System.Drawing.Point(0, 0);
            this.dgvColor.Name = "dgvColor";
            this.dgvColor.ReadOnly = true;
            this.dgvColor.Size = new System.Drawing.Size(232, 302);
            this.dgvColor.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvColor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reconfigDBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configM2MToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showM2MConnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configPackingSolutionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showPackingSolutionConnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblMold;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label lblFeature;
        private System.Windows.Forms.Label lblGrpCode;
        private System.Windows.Forms.Label lblFacility;
        private System.Windows.Forms.CheckBox chkbxSendEmail;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDepth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSeq;
        private System.Windows.Forms.ComboBox cbxColor;
        private System.Windows.Forms.ComboBox cbxFeature;
        private System.Windows.Forms.ComboBox cbxProfile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbWetCast;
        private System.Windows.Forms.RadioButton rbDryCast;
        private System.Windows.Forms.TextBox txtProjId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblUserPre;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.DataGridView dgvColor;
        private System.Windows.Forms.ToolStripMenuItem reportByProjIDToolStripMenuItem;
    }
}

