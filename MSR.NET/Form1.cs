﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using MSR.NET.Forms;
using Service.Net.Mapping;
using Service.Net.Models;
using Service.Net.tools;
using Service.Net.ViewModels;
using System.Data.Entity;


namespace MSR.NET
{
    public partial class Form1 : Form
    {
        #region <- Members ->
        private string _catalog = string.Empty;
        private readonly List<char> _aphaChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray().ToList();
        private string _currProfile;
        private string _SMTPClient = string.Empty;
        private int _featureId;
        #endregion
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //  create a new mold_mast record
            using (var data = new packingsolutionContext())
            {
                if (txtProjId.Text.Length == 0)
                {
                    SystemSounds.Beep.Play();
                    txtProjId.SelectAll();
                    txtProjId.Focus();
                    return;
                }
                if (txtSeq.Enabled)
                {
                    var next = int.Parse(txtSeq.Text);

                    if (data.MoldMast.Any(x => x.Profile.Equals(_currProfile) && x.Seq == next))
                    {
                        MessageBox.Show("ST: " + txtSeq.Text + " exists already.");
                        return;
                    }
                }

                data.MoldMast.Add(new MoldMast
                {
                    ProjId = int.Parse(txtProjId.Text),
                    Usrname = Environment.UserName,
                    Profile = _currProfile,
                    Seq = int.Parse(txtSeq.Text),
                    Drycast = rbDryCast.Checked,
                    Depth = double.Parse(txtDepth.Text),
                    Height = double.Parse(txtHeight.Text),
                    FeatureId = _featureId,
                    SysDate = DateTime.Now
                });
                data.SaveChanges();
            }
            DataFill();
        }

        public void DataFill()
        {
            Cursor = Cursors.WaitCursor;
            using (var data = new packingsolutionContext())
            {
                var tb = data.MoldMast.Include(moldMast => moldMast.Feature).Where(x => x.Profile.Equals(_currProfile.ToUpper())).ToList();
                var helper = new DataHelper();
                var dt = helper.ConvertToDataTable(tb);

                var mapperMoldMast = new DataNamesMapper<MoldMastVM>();

                var list = new List<MoldMastVM>();
                list.AddRange(mapperMoldMast.Map(dt));
                foreach (var r in list.Where(r => r.Seq > 999))
                {
                    r.Feature = tb.Single(x => x.Profile.Equals(r.Profile) && x.Seq.Equals(r.Seq)).Feature.FeatureDesc;
                    r.Mold = GetMoldRollOverName(r.Seq);
                }

                var maxSeqValue = data.MoldMast.Where(x => x.Profile.Equals(_currProfile.ToUpper())).Max(x => x.Seq);
                var nextIntValue = maxSeqValue + 1;
                var nextMoldVal = GetMoldRollOverName(nextIntValue);
                txtSeq.Text = nextIntValue.ToString();
                lblMold.Text = nextMoldVal;
                dataGridView1.DataSource = list.OrderByDescending(o => o.Seq).ToList();
                lblMold.Text = dataGridView1.Rows[0].Cells[0].Value.ToString();
            }
            Cursor = Cursors.Arrow;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //var appSetting = new AppSetting();

            //var sqlHelper = new SqlHelper(appSetting.GetConnectionString(""));
            lblUserName.Text = Environment.UserName;

            using (var data = new packingsolutionContext())
            {
                var profiles = data.Profile.Where(x => x.Profile1.Length > 0).OrderBy(x => x.Profile1).ToList();
                cbxProfile.DataSource = profiles;
                cbxProfile.DisplayMember = "Profile1";

                var features = data.MoldFeature.Where(x => x.FeatureDesc.Length > 0).OrderBy(x => x.FeatureId).ToList();
                cbxFeature.DataSource = features;
                cbxFeature.DisplayMember = "FeatureDesc";


                var colors = data.RockcastColors.Where(x => x.Color.Length > 0).OrderBy(o => o.Color).ToList();
                cbxColor.DataSource = colors;
                cbxColor.DisplayMember = "Color";
                cbxColor.ValueMember = "ColorCode";

                dgvColor.DataSource = colors.OrderBy(x => x.Color).ThenBy(x => x.ColorCode).ToList();

                var smpt = data.SmtpClientServer.FirstOrDefault(x => x.ServerName.Length > 0);
                if (smpt is null)
                {
                    MessageBox.Show("SMTP Client Server is not populated. See Admin");
                }
                else
                {
                    _SMTPClient = smpt.ServerName;
                }
            }
            TitleReset();
        }

        private string GetMoldRollOverName(int seq)
        {
            if (seq > 9999)
            {
                var sValue = seq.ToString();
                var sMoldNum = sValue.Substring(sValue.Length - 3, 3);
                var pos = int.Parse(sValue.Substring(sValue.Length - 4, 1));
                return sMoldNum + _aphaChars[pos];
            }
            return seq.ToString();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new AboutBox1();
            frm.ShowDialog(this);
        }

        private void txtProjId_TextChanged(object sender, EventArgs e)
        {
            var txt = (TextBox)sender;
            if (Numeric(txt.Text)) return;
            SystemSounds.Beep.Play();
            txtProjId.SelectAll();
            txtProjId.Focus();
        }

        private bool Numeric(string value)
        {
            var regex = new Regex("^[0-9]+$");
            return regex.IsMatch(value);
        }
        private bool Decimal(string value)
        {
            var regex = new Regex(@"^[0-9,\.]+$");
            return regex.IsMatch(value);
        }
        private void txtDepth_TextChanged(object sender, EventArgs e)
        {
            var txt = (TextBox)sender;
            if (Decimal(txt.Text)) return;
            SystemSounds.Beep.Play();
            txtDepth.SelectAll();
            txtDepth.Focus();
        }

        private void txtHeight_TextChanged(object sender, EventArgs e)
        {
            var txt = (TextBox)sender;
            if (Decimal(txt.Text)) return;
            SystemSounds.Beep.Play();
            txtHeight.SelectAll();
            txtHeight.Focus();
        }

        private void cbxProfile_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var obj = (ComboBox)sender;
            var tmp = (Profile)obj.SelectedValue;
            _currProfile = tmp.Profile1;
            // Enable manual entry of SEQ for Stretchers.

            txtSeq.Enabled = _currProfile.Equals("ST");

            using (var data = new packingsolutionContext())
            {
                var gc = data.GroupCodeXref.Single(x => x.CharGroup.Equals(_currProfile)).NumGroup;
                lblGrpCode.Text = gc.ToUpper();
            }
            DataFill();
        }

        private void cbxProfile_Leave(object sender, EventArgs e)
        {
            var obj = (ComboBox)sender;
            if (obj.Text == _currProfile) return;
            _currProfile = obj.Text;
            DataFill();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (!chkbxSendEmail.Checked) return;

            // send an email
            using (var data = new packingsolutionContext())
            {
                var list = data.MoldMast.Include(c => c.Feature).Where(x => x.Usrname == Environment.UserName && x.SysDate > DateTime.Today).ToList();
                var helper = new DataHelper();
                var dt = helper.ConvertToDataTable(list);

                var mapperMoldMast = new DataNamesMapper<MoldMastVM>();

                var vmList = new List<MoldMastVM>();
                vmList.AddRange(mapperMoldMast.Map(dt));
                foreach (var r in vmList.Where(r => r.Seq > 999))
                {
                    r.Feature = list.Single(x => x.Profile == r.Profile && x.Seq == r.Seq).Feature.FeatureDesc;
                    r.Mold = GetMoldRollOverName(r.Seq);
                }
                var ndt = helper.ConvertToDataTable(vmList);
                var body = new StringBuilder();
                body = helper.AddHTMLTableToEmailBody(ndt, ref body, true);
                if (body.Length > 20)
                {
                    SendMsg(body, "Mold numbers reserved on " + DateTime.Now);
                }
            }
        }

        private void SendMsg(StringBuilder Body, string sMsg)
        {
            if (_SMTPClient == string.Empty)
            {
                MessageBox.Show("SMTP Client Server is empty.  See Admin.  No email sent.");
                return;
            }
            var myMessage = new MailMessage();
            var sFrom = Environment.UserName;
            sFrom += "@readingrock.com";

            myMessage.IsBodyHtml = true;
            myMessage.Priority = MailPriority.Normal;
            myMessage.From = new MailAddress(sFrom, "Mold Master");
            myMessage.To.Add(sFrom);
            myMessage.Subject = sMsg;
            myMessage.Body = Body.ToString();

            using (var smtp = new SmtpClient(_SMTPClient))
            {
#if DEBUG
                // do nothing
#else
                smtp.Send(myMessage);
#endif
            }
        }

        private void reportByProjIDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (txtProjId.Text.Length == 0)
            {
                SystemSounds.Beep.Play();
                txtProjId.Focus();
                txtProjId.SelectAll();
                return;
            }
            using (var data = new packingsolutionContext())
            {
                var projId = int.Parse(txtProjId.Text);
                var list = data.MoldMast.Where(x => x.ProjId == projId).ToList();
                var helper = new DataHelper();
                var dt = helper.ConvertToDataTable(list);

                var mapperMoldMast = new DataNamesMapper<MoldMastVM>();

                var vmList = new List<MoldMastVM>();
                vmList.AddRange(mapperMoldMast.Map(dt));
                foreach (var r in vmList.Where(r => r.Seq > 999))
                {
                    r.Mold = GetMoldRollOverName(r.Seq);
                }
                var ndt = helper.ConvertToDataTable(vmList);
                var body = new StringBuilder();
                body = helper.AddHTMLTableToEmailBody(ndt, ref body, true);
                if (body.Length > 20)
                {
                    SendMsg(body, "All molds created for Project ID:  " + projId);
                }
            }
        }

        private void cbxColor_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var obj = (ComboBox)sender;
            lblColor.Text = obj.SelectedValue.ToString();
        }

        private void cbxFeature_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var obj = (ComboBox)sender;
            var feature = (MoldFeature)obj.SelectedValue;
            _featureId = feature.FeatureId;
            lblFeature.Text = feature.FeatureId.ToString();
        }

        private void TitleReset()
        {
            //var appSetting = new AppSetting();
            //var connStr = appSetting.GetConnectionString("PSEntities");

            //var connArr = connStr.Split(';');
            //GetSource(connArr, out _catalog);
            this.Text = "08 Mold Sequence Reserve v. " +
                        Assembly.GetExecutingAssembly().GetName().Version; //+ "   PS: " + _catalog.Replace("'", "");
        }
        private void GetSource(string[] ConnArr, out string Source)
        {
            Source = string.Empty;
            foreach (var el in ConnArr)
            {
                if (el.ToLower().Contains("catalog"))
                    Source = el.Split('=')[1];
            }
        }

        private void rbDryCast_CheckedChanged(object sender, EventArgs e)
        {
            lblFacility.Text = rbDryCast.Checked ? "1" : "2";
        }

        private void txtSeq_TextChanged(object sender, EventArgs e)
        {
            var txtBox = (TextBox) sender;
            if (!Numeric(txtBox.Text))
            {
                txtSeq.Focus();
                txtSeq.SelectAll();
                SystemSounds.Beep.Play();
            }
            lblMold.Text = GetMoldRollOverName(int.Parse(txtBox.Text));
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var dgv = (DataGridView)sender;
            lblMold.Text = dgv.Rows[e.RowIndex].Cells[0].Value.ToString();
        }
    }

}
